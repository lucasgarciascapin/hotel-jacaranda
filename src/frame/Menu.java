package frame;

/**
 *
 * @author Jair Gudaites Junior - RA: 21134258
 * @author Lucas Garcia Scapin - RA: 21128904
 */

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu extends JFrame {
    private boolean showMenu = false;

    public Menu() {
        super("HOTEL JACARANDÁ");
        setLayout(new FlowLayout());
        setLocationRelativeTo(null);
        setResizable(false);
        setSize(450, 120);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Register register = new Register();
        Consult consult = new Consult();

        // JPANEL
        JPanel panelRegister = new JPanel();
        JPanel panelConsult = new JPanel();

        // JBUTTON
        JButton btRegister = new JButton("REGISTER A NEW CLIENT");
        JButton btConsult = new JButton("CONSULT A CLIENT");

        panelRegister.setBorder(new TitledBorder(new EtchedBorder(), "Register"));
        panelRegister.setLayout(new FlowLayout(FlowLayout.LEFT));
        panelConsult.setBorder(new TitledBorder(new EtchedBorder(), "Consult"));
        panelConsult.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //ACTION BUTTOM SHOW REGISTER
        btRegister.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource().equals(btRegister)) {
                    register.setVisible(true);
                }
            }
        });
        panelRegister.add(btRegister);

        //ACTION BUTTOM SHOW CONSULT
        btConsult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource().equals(btConsult)) {
                    consult.setVisible(true);
                }
            }
        });
        panelConsult.add(btConsult);

        add(panelRegister);
        add(panelConsult);

    }
}
