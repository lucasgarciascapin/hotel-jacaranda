package frame;

import dao.DefaultClientDAO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Consult extends JFrame {

    public Consult() {
        super("Consult a new client");

        JPanel panelCpf = new JPanel();
        JPanel panelResult = new JPanel();
        JLabel lbCpf = new JLabel("* CPF: ");
        JLabel lbResult = new JLabel("Result:");
        JTextField tfCpf = new JTextField(15);
        JTextArea taResult = new JTextArea();
        JButton bttoConsult = new JButton("Consult");

        setLayout(new FlowLayout());
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setSize(300, 325);

        panelCpf.add(lbCpf);
        panelCpf.add(tfCpf);
        panelResult.add(lbResult);
        panelResult.add(taResult);

        panelCpf.setBounds(5, 5, 300, 25);
        add(panelCpf);
        add(panelResult);
        panelResult.setVisible(false);

        bttoConsult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String message = "";
                if (e.getSource().equals(bttoConsult)) {
                    if (verifyCpf(tfCpf.getText())) {
                        try {
                            DefaultClientDAO clientDAO = new DefaultClientDAO();
                            message = clientDAO.findClientByCpf(tfCpf.getText());
                        } catch (SQLException ex) {
                            message = ex.getMessage();
                        }
                        panelResult.setVisible(true);
                        taResult.setText(message);
                    }
                }
            }
        });
        add(bttoConsult);
    }

    private boolean verifyCpf(String cpf) {
        return !cpf.equals("");
    }
}
