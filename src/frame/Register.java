package frame;

/**
 * @author Jair Gudaites Junior - RA: 21134258
 * @author Lucas Garcia Scapin - RA: 21128904
 */

import dao.DefaultClientDAO;
import model.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Register extends JFrame {
    Client client;

    public Register() {
        super("Register Client");
        // JPANEL
        JPanel panelName = new JPanel();
        JPanel panelLastName = new JPanel();
        JPanel panelCpf = new JPanel();
        JPanel panelDateBirth = new JPanel();
        JPanel panelTelephone = new JPanel();
        JPanel panelAddress = new JPanel();
        JPanel panelEmail = new JPanel();
        // JLABEL
        JLabel lbName = new JLabel("* Name        ");
        JLabel lbLastName = new JLabel("* Last Name");
        JLabel lbCpf = new JLabel("* CPF            ");
        JLabel lbDataBirth = new JLabel("Date of Birth");
        JLabel lbTelephone = new JLabel("Telephone   ");
        JLabel lbAddress = new JLabel("Address      ");
        JLabel lbEmail = new JLabel("* Email         ");
        // JTEXTFIELD
        JTextField tfName = new JTextField(15);
        JTextField tfLastName = new JTextField(15);
        JTextField tfCpf = new JTextField(15);
        JTextField tfDataBirth = new JTextField(15);
        JTextField tfTelephone = new JTextField(15);
        JTextField tfAddress = new JTextField(15);
        JTextField tfEmail = new JTextField(15);
        // JBUTTON
        JButton bttoRegister = new JButton("Register");
        JButton btClear = new JButton("Clear");

        setLayout(new FlowLayout());
        setLocationRelativeTo(null);
        setResizable(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setSize(300, 325);
        // ADD COMPONENTS IN PANEL
        panelName.add(lbName);
        panelName.add(tfName);
        panelLastName.add(lbLastName);
        panelLastName.add(tfLastName);
        panelCpf.add(lbCpf);
        panelCpf.add(tfCpf);
        panelDateBirth.add(lbDataBirth);
        panelDateBirth.add(tfDataBirth);
        panelTelephone.add(lbTelephone);
        panelTelephone.add(tfTelephone);
        panelAddress.add(lbAddress);
        panelAddress.add(tfAddress);
        panelEmail.add(lbEmail);
        panelEmail.add(tfEmail);
        //CONFIGURATION PANEL
        panelName.setBounds(5, 5, 300, 25);
        panelLastName.setBounds(5, 35, 300, 25);
        panelCpf.setBounds(5, 65, 300, 25);
        panelDateBirth.setBounds(5, 95, 300, 25);
        panelTelephone.setBounds(5, 125, 300, 25);
        panelAddress.setBounds(5, 155, 300, 25);
        panelEmail.setBounds(5, 185, 300, 25);
        // ADD PANEL IN FRAME
        add(panelName);
        add(panelLastName);
        add(panelCpf);
        add(panelDateBirth);
        add(panelTelephone);
        add(panelAddress);
        add(panelEmail);

        //ACTION BUTTOM REGISTER
        bttoRegister.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String message;
                if (e.getSource() == bttoRegister) {
                    if (verificNotNull(tfName.getText(), tfLastName.getText(), tfCpf.getText(), tfEmail.getText())) {
                        client = new Client(tfName.getText(), tfLastName.getText(), tfCpf.getText(), tfDataBirth.getText(),
                                tfTelephone.getText(), tfAddress.getText(), tfEmail.getText());
                        DefaultClientDAO sql = new DefaultClientDAO();
                        try {
                            sql.registerClient(client);
                            message = "Client Registered";
                        } catch (SQLException ex) {
                            message = ex.getMessage();
                        }
                        JOptionPane.showMessageDialog(null, message);
                    }
                }
            }
        });
        add(bttoRegister);

        //ACTION BUTTOM CLEAR
        btClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == btClear) {
                    tfName.setText("");
                    tfLastName.setText("");
                    tfCpf.setText("");
                    tfDataBirth.setText("");
                    tfTelephone.setText("");
                    tfAddress.setText("");
                    tfEmail.setText("");
                }
            }
        });
        add(btClear);
    }

    // VERIFICATION NOT NULL
    public boolean verificNotNull(String name, String lastName, String cpf, String email) {
        if (!name.equalsIgnoreCase("")) {
            if (!lastName.equalsIgnoreCase("")) {
                if (!cpf.equalsIgnoreCase("")) {
                    if (!email.equalsIgnoreCase("")) {
                        return true;
                    } else {
                        JOptionPane.showMessageDialog(null, "E-mail is Empty");
                        return false;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "CPF is Empty");
                    return false;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Last Name is Empty");
                return false;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Name is Empty");
            return false;
        }
    }

}
