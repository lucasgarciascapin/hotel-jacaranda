package service;

/**
 * @author Jair Gudaites Junior - RA: 21134258
 * @author Lucas Garcia Scapin - RA: 21128904
 * @author Matheus Oliveira da Mata - RA: 21073117
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection implements AutoCloseable {

    private final String databaseName = "HotelJacaranda";

    public Connection getConnectionMySQL() throws RuntimeException {

        Connection connection = null;

        try {
            String url = "jdbc:mysql://localhost:3306/" + databaseName;
            String username = "root";
            String password = "root";

            Class.forName("com.mysql.jdbc.Driver");

            connection = DriverManager.getConnection(url, username, password);

            if (connection != null) {
                System.out.println("Database -> CONNECTED (:");
            } else {
                System.out.println("Database -> NOT CONNECTED ):");
            }

        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Specified driver could not be found..");
        } catch (SQLException e) {
            throw new RuntimeException("Unable to connect to Database.");
        }

        return connection;
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing connection.");
    }

    public String getDatabaseName() {
        return databaseName;
    }
}
