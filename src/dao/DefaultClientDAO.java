package dao;

/**
 * @author Jair Gudaites Junior - RA: 21134258
 * @author Lucas Garcia Scapin - RA: 21128904
 * @author Matheus Oliveira da Mata - RA: 21073117
 */

import model.Client;
import service.MySQLConnection;

import java.sql.*;

public class DefaultClientDAO {

    private MySQLConnection sqlConnection = new MySQLConnection();
    private Connection connection = sqlConnection.getConnectionMySQL();
    private static String tableName = "clients";

    public int createTableClients() throws SQLException {
        String query = "CREATE TABLE " + tableName + "(" +
                "name VARCHAR(100) NOT NULL," +
                "lastName VARCHAR(100) NOT NULL," +
                "cpf CHAR(11) PRIMARY KEY," +
                "dateBirth DATE," +
                "telephone CHAR(11)," +
                "address VARCHAR(100)," +
                "email VARCHAR(100) NOT NULL);";

        return executeQuery(query);
    }

    public String findClientByCpf(String cpf) throws SQLException {
        String query = "SELECT * FROM " + tableName + " WHERE cpf = " + cpf + ";";
        return executeSelectQuery(query);
    }

    public int registerClient(Client client) throws SQLException {
        String query = "INSERT INTO " + tableName +
                "(name, lastName, cpf, dateBirth, telephone, address, email)" +
                " VALUES ('" + client.getName() + "'" + "," + "'" + client.getLastName() + "','" + client.getCpf() + "','"
                + client.getBirthDate() + "','" + client.getTelephone() + "','" + client.getAddress() + "','"
                + client.getEmail() + "');";

        return executeQuery(query);
    }

    private int executeQuery(String query) throws SQLException {
        Statement statement = connection.createStatement();
        return statement.executeUpdate(query);
    }

    private String executeSelectQuery(String query) throws SQLException {
        String result = "Client not found, please try again!";
        Statement statement = connection.createStatement();
        try {
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();

            String name = resultSet.getString(1) + " " + resultSet.getString(2) + "\n";
            String cpf = resultSet.getString(3) + "\n";
            String birthDate = resultSet.getString(4) + "\n";
            String telephone = resultSet.getString(5) + "\n";
            String address = resultSet.getString(6) + "\n";
            String email = resultSet.getString(7) + "\n";

            result = "Name: " + name +
                    "CPF: " + cpf +
                    "BirthDate: " + birthDate +
                    "Telephone: " + telephone +
                    "Address: " + address +
                    "E-mail: " + email;

        } catch (SQLException e) {
            return result;
        }
        return result;
    }
}
