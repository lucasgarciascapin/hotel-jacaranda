
/**
 * @author Jair Gudaites Junior - RA: 21134258
 */

import frame.Menu;
import service.MySQLConnection;

/**
 *  @author Jair Gudaites Junior - RA: 21134258
 *  @author Lucas Garcia Scapin - RA: 21128904
 *  @author Matheus Oliveira da Mata - RA: 21073117
 */

public class App_hotelJacaranda {

    public static void main(String[] args) {

//                CREATE A TABLE
//        try{
//            new DefaultClientDAO().createTableClients();
//            JOptionPane.showMessageDialog(null,"Table Clients Created Successfully!");
//        } catch (SQLException ex){
//            JOptionPane.showMessageDialog(null,"IT WAS NOT POSSIBLE TO CREATE A TABLE\n"+ex.getMessage());
//        }

        MySQLConnection connection = new MySQLConnection();
        connection.getConnectionMySQL();

        new Menu().setVisible(true);
    }
}
