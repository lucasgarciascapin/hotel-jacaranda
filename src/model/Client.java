package model;

/**
 * @author Jair Gudaites Junior - RA: 21134258
 * @author Lucas Garcia Scapin - RA: 21128904
 */

// TODO JAIR
public class Client {

    private String name;
    private String lastName;
    private String cpf;
    private String birthDate;
    private String telephone;
    private String address;
    private String email;

    public Client() {
    }

    public Client(String name, String lastName, String cpf, String birthDate,
                  String telephone, String address, String email) {
        if (name != null) {
            this.name = name;
        }
        if (lastName != null) {
            this.lastName = lastName;
        }
        if (cpf != null) {
            this.cpf = cpf;
        }
        this.birthDate = birthDate;
        this.telephone = telephone;
        this.address = address;
        if (email != null) {
            this.email = email;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
